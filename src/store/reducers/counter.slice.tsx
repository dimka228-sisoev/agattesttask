import { createSlice } from '@reduxjs/toolkit';

export const counterSlice = createSlice({
    name: 'counter',
    initialState: 0,
    reducers: {
        inc: (state) => {
            state++;
            return state;
        },
        setCounter: (state, { payload }) => {
            state = payload;
            return state;
        },
    }
});

export const {actions: counterActions, reducer: counterReducer} = counterSlice;