import { createSlice } from '@reduxjs/toolkit';
import L from "leaflet";

export type Marker = {
    markerRef: L.Point | L.Polygon,
    markerData: {
        type: string;
        properties: {
            id: number;
            name: string;
        },
        geometry: {
            coordinates: [
                [
                    [
                        number,
                        number,
                    ],
                ],
            ],
            type: string;
        }
    }
}

const initialState: Marker[] = [];

export const markersSlice = createSlice({
    name: 'markers',
    initialState,
    reducers: {
        // setMarkers: (state, { payload }: {payload: Marker[]}) => {
        //     state.length = 0;
        //     payload.map((marker) => state.push(marker));
        // },
        deleteMarker: (state, { payload }) => {
            for (let i = 0; i < state.length; i++) {
                if (state[i].markerData.properties.id === payload) {
                    state.splice(i, 1);
                    break;
                }
            }
        },
        addPoint: (state, { payload }) => {
            state.push({
                markerRef: payload.marker,
                markerData: {
                    type: "Feature",
                    properties: {
                        id: payload.id + 1,
                        name: payload.name,
                    },
                    geometry: {
                        coordinates: payload.coordinates,
                        type: "Point",
                    }
                }
            })
        },
        addPolygon: (state, { payload }) => {
            state.push({
                markerRef: payload.marker,
                markerData: {
                    type: "Feature",
                    properties: {
                        id: payload.id + 1,
                        name: payload.name,
                    },
                    geometry: {
                        coordinates: payload.coordinates,
                        type: "Polygon",
                    }
                }
            })
        }
    }
});

export const {actions: markerActions, reducer: markerReducer} = markersSlice;