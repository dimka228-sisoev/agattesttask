import { markerReducer } from './reducers/markers.slice';
import { counterReducer } from './reducers/counter.slice';
import { combineReducers, configureStore } from '@reduxjs/toolkit';

const reducers = combineReducers({
    markersData: markerReducer,
    counter: counterReducer,
})

export const store = configureStore({
    reducer: reducers,
    middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch