import { useState } from "react";
import L from "leaflet";
import "./style.css";
import { useDispatch, useSelector } from "react-redux";
import { markerActions } from './store/reducers/markers.slice';
import { counterActions } from "./store/reducers/counter.slice";
import React from "react";

function NewMarker(props: { map: L.Map }) {

  const dispatch = useDispatch()

  const counter = useSelector((state: { counter: number }) => state.counter);

  const { map } = props;

  const [name, setName] = useState("");
  const [width, setWidth] = useState("");
  const [height, setHeight] = useState("");
  const [markerType, setMarkerType] = useState("point");
  const [coordinates, setCoordinates] = useState<[number, number][]>([]);

  const addPoint = async () => {
    if (
      !!name &&
      (
        parseFloat(width) >= -90 && parseFloat(width) <= 90
      ) && (
        parseFloat(height) >= -180 && parseFloat(height) <= 180
      )
    ) {
      const marker = L.marker([+width, +height]);
      console.log(marker);
      marker.addTo(map);
      dispatch(markerActions.addPoint({
        name: name,
        coordinates: [
          width,
          height
        ],
        id: counter,
        marker: marker
      }));
      dispatch(counterActions.inc());
    } else {
      alert("Проверьте правильность заполнения полей");
    }
  }

  const addPolygon = async () => {
    const polygon = L.polygon(coordinates, { color: 'red' }).addTo(map);
    dispatch(markerActions.addPolygon({ 
      name: name,
      coordinates: [ coordinates ],
      id: counter,
      marker: polygon
    }));
    dispatch(counterActions.inc());
    setCoordinates([]);
  }

  const addPointToPolygon = async () => {
    if (!!name && !isNaN(+width) && !isNaN(+height)) {
      setCoordinates([...coordinates, [+width, +height]]);
    } else {
      alert("Проверьте правильность заполнения полей");
    }
  }

  return (
    <div className="add-new-element">
      <div className="input-row">
        Тип метки
        <select className="input-field" defaultValue={markerType} onChange={(e) => setMarkerType(e.target.value)}>
          <option value="point">Точка</option>
          <option value="polygon">Полигон</option>
        </select>
      </div>
      <div className="input-row">
        Имя
        <input className="input-field" value={name} onChange={(e) => setName(e.target.value)}></input>
      </div>
      <div className="input-row">
        Широта
        <input className="input-field" value={width} onChange={(e) => setWidth(e.target.value)}></input>
      </div>
      <div className="input-row">
        Долгота
        <input className="input-field" value={height} onChange={(e) => setHeight(e.target.value)}></input>
      </div>
      {markerType === "polygon" &&
        <>
          <button className="add-button" onClick={addPointToPolygon}>Добавить координату</button>
          {coordinates.length > 0 &&
            <div className="coordinates-container">
              <div className="coordinates-title">Координаты</div>
              {coordinates.map((coordinate, key) => 
                <div key={key} className="coordinates-row">
                  <div className="coordinates-column">{coordinate[0]}</div>
                  <div className="coordinates-column">{coordinate[1]}</div>
                </div>
              )}
            </div>
          }
        </>
      }
      {((coordinates.length > 1 && markerType === "polygon") || markerType === "point") &&
        <button className="add-button" onClick={markerType === "point" ? addPoint : addPolygon}>Сохранить</button>
      }
    </div>
  )
}

export default NewMarker;
