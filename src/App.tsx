import { useEffect, useState } from "react";
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import "./style.css";
import MarkerRow from "./MarkerRow";
import { useDispatch, useSelector } from "react-redux";
import { Marker, markerActions } from './store/reducers/markers.slice';
import NewMarker from "./NewMarker";
import { counterActions } from "./store/reducers/counter.slice";
import React from "react";

function App() {

  const markers = useSelector((state: { markersData: Marker[] }) => state.markersData);

  const dispatch = useDispatch()

  const [globalMap, setGlobalMap] = useState<L.Map>();

  useEffect(() => {
    const getShapes = async () => {
      try {
        return await fetch("/shapes.json").then((res) => res.json());
      } catch (e) {
        alert(e);
        return [];
      }
    }

    const map = L.map("map-id", { zoomControl: false }).setView(
      [51.505, -0.09],
      13
    );

    setGlobalMap(map);

    getShapes()
    .then((res) => {
      let count = 0;
      for (let i = 0; i < res.length; i++) {
        if (res[i].geometry.type === "Point") {
          const marker = L.marker(res[i].geometry.coordinates).addTo(map);
          dispatch(markerActions.addPoint({
            name: res[i].properties.name,
            coordinates: res[i].geometry.coordinates,
            id: count,
            marker: marker
          }));
        } else if (res[i].geometry.type === "Polygon") {
          const polygon = L.polygon(res[i].geometry.coordinates[0], { color: 'red' }).addTo(map);
          dispatch(markerActions.addPolygon({ 
            name: res[i].properties.name,
            coordinates: res[i].geometry.coordinates,
            id: count,
            marker: polygon
          }));
        }
        count++;
      }
      dispatch(counterActions.setCounter(10));
      return res;
    })

    L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(map);
  }, []);

  return (
    <>
      <div className="left-menu">
        <div className="element-list">
          {
            markers.length ?
            markers.map((marker) =>
              <MarkerRow marker={marker} key={marker.markerData.properties.id} map={globalMap} />
            )
            :
            <div className="empty-text">Список меток пуст</div>
          }
        </div>
        <NewMarker map={globalMap} />
      </div>
      <div id="map-id"></div>;
    </>
  )
}

export default App;
