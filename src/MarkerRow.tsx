import React from "react";
import L from "leaflet";
import { useDispatch } from "react-redux";
import { markerActions } from './store/reducers/markers.slice';
import { Marker } from "./store/reducers/markers.slice";

function MarkerRow(props: { marker: Marker, map: L.Map }) {

    const dispatch = useDispatch()

    const { marker, map } = props;

    const deleteMarker = async () => {
        dispatch(markerActions.deleteMarker(marker.markerData.properties.id));
        map.removeLayer(marker.markerRef);
    }
  
    return (
        <div className="element-row" key={marker.markerData.properties.id}>
            <div className="colomn-in-row">
                {marker.markerData.properties.name}
            </div>
            <div className="colomn-in-row">
                <button className="delete-button" onClick={deleteMarker}>
                    Удалить
                </button>
            </div>
        </div>
    )
  }
  
  export default MarkerRow;
  